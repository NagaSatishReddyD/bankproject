-module(bank).

-export([create_bank_process/3]).

create_bank_process(Master, Name, MoneyContains)->
	receive_messages(Master, Name, MoneyContains).

receive_messages(Master, Name, MoneyContains)->
	{_, NamePid} = erlang:process_info(self(), registered_name),
	receive
		{requestbank, FromCustomer, Amount} ->
			if 
				Amount =< MoneyContains ->
					Master ! {approved, FromCustomer, NamePid, Amount},
					AmountLeft = MoneyContains - Amount,
					FromCustomer ! {approved, Amount},
					receive_messages(Master, Name, AmountLeft);
				true ->
					Master ! {rejected, Name, FromCustomer, Amount},
					FromCustomer ! {rejected, self()},
					receive_messages(Master,Name, MoneyContains)
			end
	after
		2000 -> Master ! {printbank, Name, MoneyContains}
	end.
	