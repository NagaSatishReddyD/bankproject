-module(customer).

-import(lists, [nth/2]).

-export([create_customer/4]).

create_customer(Master, Name, MoneyContains, BankPidList)->
	timer:sleep(500),
	InitialMoney = MoneyContains,
	process_starts(Master, Name, MoneyContains, BankPidList, InitialMoney).

process_starts(Master, Name, MoneyContains, BankPidList, InitialMoney) ->
	BanksLength = length(BankPidList),
	IsMoneyZero = MoneyContains /= 0,
	IsAllBanksRejected = BanksLength /= 0,
	if
		IsMoneyZero and IsAllBanksRejected ->
			RandomBank = rand:uniform(BanksLength),
			BankPid = nth(RandomBank, BankPidList),
			RandomAmount = rand:uniform(50),
			if
				RandomAmount =< MoneyContains ->
					{_, FromCustomer} = erlang:process_info(self(), registered_name),
					{_, BankName} = erlang:process_info(BankPid, registered_name),
					Master ! {request, FromCustomer, BankName, RandomAmount},
					BankPid ! {requestbank, FromCustomer, RandomAmount},
					receive_messages(Master, Name, MoneyContains, BankPidList, InitialMoney);
				true ->
					process_starts(Master, Name, MoneyContains, BankPidList, InitialMoney)
			end;
		true ->
			receive_messages(Master, Name, MoneyContains, BankPidList, InitialMoney)
		
	end.
	
receive_messages(Master, Name, MoneyContains, BankPidList, InitialMoney)->
	receive
		{approved, Amount} ->
			AmountLeft = MoneyContains - Amount,
			process_starts(Master, Name, AmountLeft, BankPidList, InitialMoney);
		{rejected, BankPid} ->
			BankRemainedList = lists:delete(BankPid, BankPidList),
			process_starts(Master,Name, MoneyContains, BankRemainedList, InitialMoney)
	after
		2000 -> Master ! {printcustomer, Name, MoneyContains, InitialMoney}
	end.