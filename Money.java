import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Money {

	public static void main(String[] args) throws IOException, InterruptedException {
		File customersFile = new File(System.getProperty("user.dir")+"/customers.txt");
		File bankFile = new File(System.getProperty("user.dir")+"/banks.txt");
		List<Bank> banksList = new ArrayList<>();
		List<Customer> customersList = new ArrayList<>();
		getData(customersFile, customersList, false);
		getData(bankFile, banksList, true);
		printInitalResult(customersList, banksList);
		List<Bank> bankObjects = new ArrayList<>();
		List<Thread> bankThreadList = new ArrayList<>();
		List<Thread> customerThreadList = new ArrayList<>();
		for(int i = 0; i < banksList.size(); i++) {
			Bank bank = banksList.get(i);
			bankObjects.add(bank);
			Thread thread = new Thread(bank);
			bankThreadList.add(thread);
			thread.start();
		}
		customersList.stream().forEach(customer -> {
			customer.addBanksList(bankObjects);
			Thread thread = new Thread(customer);
			customerThreadList.add(thread);
			thread.start();
		});
		while(!customerThreadList.isEmpty()) {
			Thread  thread = customerThreadList.get(0);
			if(!thread.isAlive())
				customerThreadList.remove(0);
		}
		printFinalResult(customersList, banksList);
		System.exit(0);
	}
	
	private static void printFinalResult(List<Customer> customersList, List<Bank> banksList) {
		customersList.stream().forEach(customer -> {
			if(customer.getMoney() == 0) {
				System.out.println(customer.getName()+" has been reached the objective of "+customer.getInitialMoney()+". Woo Hoo!");
			}else {
				System.out.println(customer.getName()+" was only able to borrow "+customer.getBorrowedMoney()+". Boo Hoo!");
			}
		});
		
		banksList.stream().forEach(bank -> {
			System.out.println(bank.getName()+" has "+bank.getMoney()+" dollar(s) remaining.");
		});
	}
	
	private static void printInitalResult(List<Customer> customersList, List<Bank> banksList) {
		System.out.println("** Customers and Loan Objectives **");
		customersList.stream().forEach(customer ->{
			System.out.println(customer.getName() +":"+customer.getMoney());
		});
		System.out.println();
		System.out.println("** Banks and financial Resources **");
		banksList.stream().forEach(bank -> {
			System.out.println(bank.getName() +":"+bank.getMoney());
		});
	}
	
	@SuppressWarnings("unchecked")
	private static <E> void getData(File customersFile, List<E> dataList, boolean isBank) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(customersFile));
		String fileData = "";
		String line = "";
		while((line = reader.readLine()) != null) {
			fileData = fileData.concat(line.trim());
		}
		reader.close();
		fileData = fileData.replace(".", "");
		String[] individualDatasArray = fileData.split("}");
		int noOfCustomers = individualDatasArray.length;
		int index = 0;
		while(index < noOfCustomers) {
			String individualData = individualDatasArray[index];
			String name =  individualData.substring(individualData.indexOf("{")+1, individualData.indexOf(",")).trim();
			int money = Integer.parseInt(individualData.substring(individualData.indexOf(",")+1, individualData.length()).trim());
			E data;
			if(isBank) {
				data =  (E) new Bank(name, money);
			}else {
				data =  (E) new Customer(name, money);
			}
			dataList.add(data);
			index++;
		}
	}
}
