-module(money). 

-import(file, [consult/1]).
-import(lists,[nth/2, foreach/2, append/2]).
-import(string, [concat/2]).

-import(customer, [create_customer/4]).
-import(bank, [create_bank_process/3]).

-export([start/0]). 

start() -> 
	{ok, Customer_Data} = file:consult("customers.txt"),
	{ok, Banks_Data} = file:consult("banks.txt"),
	print_data(Customer_Data, Banks_Data),
	BankNames = create_bank_process_ids(Banks_Data, []),
	create_customer_process(Customer_Data, BankNames),
	master().

create_customer_process([Customer | Tail],BankName)->
	Name = element(1, Customer),
	MoneyContains = element(2, Customer),
	Pid = spawn(customer, create_customer, [self(), Name, MoneyContains, BankName]),
	register(Name, Pid),
	create_customer_process(Tail, BankName) ;
create_customer_process([],_) -> ok.

create_bank_process_ids([Bank | Tail], Result) ->
	Name = element(1, Bank),
	MoneyContains = element(2, Bank),
	Pid = spawn(bank, create_bank_process, [self(), Name, MoneyContains]),
	register(Name, Pid),
	create_bank_process_ids(Tail, Result ++ [Pid]) ;
create_bank_process_ids([], Result) -> Result.

print_data(Customer_Data, Banks_Data)->
	PrintData = fun(Elem)->
			Name = element(1, Elem),
			MoneyContains = element(2, Elem),
			io:fwrite("~p:~w~n", [Name, MoneyContains])
		end,
		
	io:fwrite("**Customers and Loan Objectives**~n"),
	lists:foreach(PrintData, Customer_Data),
	io:fwrite("** Banks and financial Resources **~n"),
	lists:foreach(PrintData, Banks_Data).
	
master()->
	receive
		{request, Customer, BankName, Amount} ->
			io:format("~p requests a loan of ~p dollars from ~p ~n", [Customer, Amount, BankName]), 			
			master();
		{approved, Customer, BankName, Amount} ->
			io:format("~p approves a loan of ~p dollars to ~p ~n", [BankName, Amount, Customer]),
			master();
		{rejected, BankName, Customer, Amount} ->
			io:format("~p rejected a loan of ~p dollars to ~p ~n", [BankName, Amount, Customer]),
			master();
		{printbank, Name, Amount} ->
			io:format("~p has only ~p dollars remaining~n",[Name, Amount]),
			master();
		{printcustomer, Name, MoneyContains, InitialMoney}->
			IsMoneyZero = MoneyContains == 0,
			if 
				IsMoneyZero ->
					io:fwrite("~p has reached objective of ~p dollars. Woo Hoo!~n", [Name, InitialMoney]),
					master();
				true ->
					AmountBorrowed = InitialMoney - MoneyContains,
					io:fwrite("~p was only able to borrow ~p dollars. Boo Hoo!~n", [Name, AmountBorrowed]),
					master()
			end
	after 
		5500 -> ok
	end.
