import java.util.concurrent.ArrayBlockingQueue;

public class Bank implements Runnable {

	private String name;
	private int money;
	private ArrayBlockingQueue<Customer> customerRequests = new ArrayBlockingQueue<>(10);
	private boolean running;

	public Bank(String name, int money) {
		this.name = name;
		this.money = money;
		this.running = true;
	}
	
	public void stopBank() {
		this.running = false;
	}

	@Override
	public void run() {
		synchronized (this) {
			try {
				while(this.running) {
					Customer customer = customerRequests.take();
					synchronized (customer) {
						int customerRequestAmount = customer.getAmountRequested();
						if(this.money >= customerRequestAmount) {
							customer.setRequestResult(true);
							this.money -= customerRequestAmount;
						}else {
							customer.setRequestResult(false);
						}
						customer.notify();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendRequest(Customer customer) {
		try {
			this.customerRequests.put(customer);
		} catch (InterruptedException e) {
			System.out.println("Adding Interrpution");
		}
	}

	public String getName() {
		return this.name;
	}

	public int getMoney() {
		return this.money;
	}

}
