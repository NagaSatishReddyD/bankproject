import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Customer implements Runnable {

	private String name;
	private int money;
	private List<Bank> banksList;
	private int initialAmount;
	private int amountToBeRequested;
	private boolean requestResult;

	public Customer(String name, int money) {
		this.name = name;
		this.money = money;
		this.initialAmount = money;
		this.banksList = new ArrayList<>();
		this.amountToBeRequested = 0;
		this.requestResult = false;
	}

	public void addBanksList(List<Bank> bankObjects) {
		this.banksList.addAll(bankObjects);
	}

	@Override
	public void run() {
		Random random = new Random();
		synchronized (this) {
			try {
				while(!banksList.isEmpty() && this.money != 0) {
					Thread.sleep(random.nextInt(90)+11);
					this.amountToBeRequested = random.nextInt(getMoneyValueLimit()) + 1;
					Bank bank = this.banksList.get(random.nextInt(this.banksList.size()));
					bank.sendRequest(this);
					System.out.println(this.name+" requests a loan of "+amountToBeRequested+" dollar(s) from "+bank.getName());
					this.wait();
					if(this.requestResult) {
						this.money -= amountToBeRequested;
						System.out.println(bank.getName()+ " approves a loan of " + amountToBeRequested +" dollar(s) to "+this.name);
					}else {
						this.banksList.remove(bank);
						System.out.println(bank.getName()+ " rejects a loan of " + amountToBeRequested +" dollar(s) to "+this.name);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private int getMoneyValueLimit() {
		return this.money >= 50 ? 50: this.money;
	}

	public int getAmountRequested() {
		return this.amountToBeRequested;
	}

	public void setRequestResult(boolean result) {
		this.requestResult = result;
	}

	public String getName() {
		return this.name;
	}

	public int getInitialMoney() {
		return this.initialAmount;
	}

	public int getMoney() {
		return this.money;
	}

	public int getBorrowedMoney() {
		return this.initialAmount - this.money;
	}

}
